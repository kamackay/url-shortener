package main

import (
	"gitlab.com/kamackay/url-shortener/server"
)

func main() {
	server.New().Start()
}
