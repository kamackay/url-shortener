package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/url-shortener/babble"
)

type Server struct {
	log        *logrus.Logger
	engine     *gin.Engine
	mux        sync.Mutex
	cronRunner *cron.Cron
	babbler    *babble.Babbler
}

func New() *Server {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		ForceColors:   true,
	})
	babbler := babble.New(&babble.Config{
		Path:      "/words.txt",
		Separator: "-",
		Count:     3,
	})
	return &Server{
		log:        log,
		engine:     gin.Default(),
		cronRunner: cron.New(),
		babbler:    &babbler,
	}
}

func (this *Server) getNewId() string {
	return this.babbler.Babble()
}

func (this *Server) Start() {
	this.engine.Use(gzip.Gzip(gzip.BestCompression))
	this.engine.Use(location.Default())
	this.engine.Use(cors.Default())

	this.engine.GET("/*root", func(ctx *gin.Context) {
		path := strings.Trim(ctx.Request.URL.Path, "/")
		if len(path) == 0 {
			if ctx.GetHeader("Authorization") == "plz" {
				this.log.Info("Returning all configs")
				config := this.readConfig().toJson()
				this.log.Infof("Config: %+v", config)
				ctx.JSON(http.StatusOK, config)
				return
			}
			ctx.String(http.StatusOK, "Hey! This is a URL shortener\n")
			return
		}
		config := this.readConfig()
		urlConfig := config.Urls[path]
		if urlConfig == nil {
			ctx.String(http.StatusNotFound, "Nothing Found at this URL\n")
		} else {
			ctx.Redirect(http.StatusTemporaryRedirect, urlConfig.Url)

			urlConfig.TimesCalled++
			this.syncConfig(config)
		}
	})

	this.engine.PUT("/", this.addEndpoint)
	this.engine.POST("/", this.addEndpoint)
	this.engine.DELETE("/*root", func(ctx *gin.Context) {
		path := strings.Trim(ctx.Request.URL.Path, "/")
		if len(path) == 0 {
			ctx.String(http.StatusOK, "Hey! This is a URL shortener\n")
			return
		}
		config := this.readConfig()
		urlConfig := config.Urls[path]
		if urlConfig == nil {
			ctx.String(http.StatusNotFound, "Nothing Found at this URL\n")
		} else {
			delete(config.Urls, path)
			this.syncConfig(config)
			ctx.String(http.StatusOK, fmt.Sprintf("'%s' successfully deleted\n", path))
		}
	})

	if err := this.engine.Run(fmt.Sprintf(":%s", os.Getenv("PORT"))); err != nil {
		panic(err)
	} else {
		this.log.Info("Successfully Started Server")
	}
}

func (this *Server) addEndpoint(ctx *gin.Context) {
	url := location.Get(ctx)
	body := ctx.Request.Body
	bytes, _ := ioutil.ReadAll(body)
	if err := body.Close(); err != nil {
		this.log.Warnf("Could not Close Stream: %v", err)
	}

	newUrl := string(bytes)

	config := this.readConfig()

	for id, c := range config.Urls {
		if c.Url == newUrl {
			ctx.String(http.StatusOK, fmt.Sprintf("%s://%s/%s", url.Scheme, ctx.Request.Host, id)+"\n")
			return
		}
	}

	id := this.getNewId()
	config.Urls[id] = &UrlConfig{
		Id:          id,
		Url:         newUrl,
		CreateTime:  time.Now().UnixNano(),
		TimesCalled: 0,
	}

	this.syncConfig(config)

	ctx.String(http.StatusCreated, fmt.Sprintf("%s://%s/%s", url.Scheme, ctx.Request.Host, id)+"\n")
}

func (this *Server) readConfig() *Config {
	this.mux.Lock()
	defer this.mux.Unlock()
	var config Config
	if bytes, err := ioutil.ReadFile("/config/urls.json"); err != nil {
		this.log.Warnf("Could Not Read Config %v", err)
		return defaultConfig()
	} else if err := json.Unmarshal(bytes, &config); err != nil {
		this.log.Warnf("Could not Unmarshal Config Object %v", err)
		return defaultConfig()
	}
	this.log.Debugf("Successfully Read Config File: %+v", config)
	return &config
}

func (this *Server) syncConfig(config *Config) {
	this.mux.Lock()
	defer this.mux.Unlock()
	if bytes, err := json.Marshal(config); err != nil {
		this.log.Warnf("Could not Marshal Config Object %v", err)
	} else if err := ioutil.WriteFile("/config/urls.json", bytes, 0666); err != nil {
		this.log.Warnf("Could Not Write Config %v", err)
	}
}

func defaultConfig() *Config {
	return &Config{
		Urls: make(UrlMap, 0),
	}
}

type UrlMap = map[string]*UrlConfig

type Config struct {
	Urls UrlMap `json:"urls"`
}

type UrlConfig struct {
	Id          string `json:"id"`
	Url         string `json:"url"`
	CreateTime  int64  `json:"createTime"`
	TimesCalled int64  `json:"timesCalled"`
}

func (c Config) toJson() map[string]map[string]interface{} {
	j := make(map[string]map[string]interface{})
	for key, val := range c.Urls {
		if val == nil || len(key) == 0 {
			continue
		}
		item := make(map[string]interface{})
		item["id"] = val.Id
		item["url"] = val.Url
		item["createTime"] = val.CreateTime
		item["timesCalled"] = val.TimesCalled
		j[key] = item
	}
	return j
}
