module gitlab.com/kamackay/url-shortener

go 1.13

require (
	github.com/bytedance/sonic v1.8.2 // indirect
	//github.com/gin-contrib/cache v1.1.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.1
	github.com/gin-contrib/location v0.0.2
	github.com/gin-gonic/gin v1.9.0
	github.com/klauspost/cpuid/v2 v2.2.4 // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/ugorji/go/codec v1.2.10 // indirect
	golang.org/x/arch v0.2.0 // indirect
	golang.org/x/crypto v0.6.0 // indirect
)
