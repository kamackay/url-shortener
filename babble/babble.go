package babble

import (
	"math/rand"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Babbler struct {
	Count     int
	Separator string
	Words     []string
}

func New(config *Config) Babbler {
	return Babbler{
		Count:     optionalInt(&config.Count, 2),
		Separator: optionalString(&config.Separator, "-"),
		Words:     readAvailableDictionary(optionalString(&config.Path, "~/words.txt")),
	}
}

func optionalString(cond *string, defaultVal string) string {
	if cond == nil || len(*cond) == 0 {
		return defaultVal
	} else {
		return *cond
	}
}

func optionalInt(cond *int, defaultVal int) int {
	if cond == nil || *cond == 0 {
		return defaultVal
	} else {
		return *cond
	}
}

func (this Babbler) Babble() string {
	pieces := make([]string, 0)
	for i := 0; i < this.Count; i++ {
		pieces = append(pieces, this.Words[rand.Int()%len(this.Words)])
	}

	return strings.Join(pieces, this.Separator)
}

type Config struct {
	Path      string
	Separator string
	Count     int
}
