IMAGE=docker.keith.sh/url-shortener:$1

time docker build . -t "$IMAGE" && \
    # docker-squash "$IMAGE" --tag "$IMAGE" && \
    docker push "$IMAGE" && \
    kubectl --context do-nyc3-keithmackay-cluster -n url-shortener \
      set image sts/url-shortener server=$IMAGE && \
    kubectl --context do-nyc3-keithmackay-cluster -n url-shortener rollout restart statefulset url-shortener

sleep 1
ATTEMPTS=0
ROLLOUT_STATUS_CMD="kubectl --context do-nyc3-keithmackay-cluster rollout status sts/url-shortener -n url-shortener"
until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 60 ]; do
  $ROLLOUT_STATUS_CMD
  ATTEMPTS=$((ATTEMPTS + 1))
  sleep 1
done

ECHO "Successfully deployed" $1
